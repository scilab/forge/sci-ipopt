// ====================================================================
// Copyright Yann COLLETTE 2009
// This file is released into the public domain
// ====================================================================

help_lang_dir = get_absolute_file_path('build_help.sce');

tbx_build_help("SciIPOpt", help_lang_dir);

clear help_lang_dir;
